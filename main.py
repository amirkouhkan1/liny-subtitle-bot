#-*-coding: utf-8 -*-

# -------- SQLAlchemy ----------#
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from DB import Users, engine, Search
# -------- Telepot --------------#
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import ReplyKeyboardMarkup
# ---------- Other --------------#
from time import sleep
# ----------- Seach Engine -------#
from search import WorldSubtitle, Subtitlepedia

DBSession = sessionmaker(bind=engine)
session = DBSession()

TOKEN = '627018280:AAHYbZInLper55p7ew9cmLf9bKNSCHmjkXc'
bot = telepot.Bot(TOKEN)

def handle(msg):
    msg_id = msg['message_id']
    chatid = msg['chat']['id']
    cmd = msg['text']

    subtitle = 0
    world = 0


    if cmd.startswith('/start'):
       bot.sendMessage(chatid, "به ربات زیرنویس یاب خوش آمدید\nبرای جستجوی زیرنویس تنها کافیست نام فیلم یا سریال را جستجو کنید\n\n\nربات در حال تکمیل شدن می‌باشد...")
        
    else:
        try:
            check_user = session.query(Users).filter(Users.chatid == chatid).one()
            if check_user:
                search_movie = WorldSubtitle(cmd)
                world_movie_name = search_movie.search_name()
                if len(world_movie_name)<=0:
                    world = 0
                else:
                    world = 1
                search_movie = Subtitlepedia(cmd)
                subtitlepedia_movie_name = search_movie.search_name()

                if len(subtitlepedia_movie_name)<=0:
                    subtitle = 0
                else:
                    subtitle = 1

                if world==0 and subtitle==0:
                    bot.sendMessage(chatid, "متاسفانه زیرنویستون رو پیدا نکردم 😥\nداخل آپدیت‌های بعدی سعی میکنم همه‌ی زیرنویس‌هات رو پیدا کنم😍\n\n@searchsubbot", reply_to_message_id=msg_id)

                if world==1:
                    msg_on = 1
                    msg_off = 0
                    movie_links = WorldSubtitle(cmd)
                    movie_download = movie_links.search_link()
                    keyboard = [world_movie_name[0:1],
                                world_movie_name[1:2], 
                                world_movie_name[2:3], 
                                world_movie_name[3:4], 
                                world_movie_name[4:5], 
                                world_movie_name[5:6],
                                 world_movie_name[6:len(world_movie_name)]]
                    list_movies = ReplyKeyboardMarkup(keyboard=keyboard)
                    if msg_on==1 and msg_off==0:
                        bot.sendMessage(chatid, "🔥🔥🔥لیست اسم فیلم‌هایی که شبیه به فیلمت هستن 🔥🔥🔥",
                                        reply_to_message_id=msg_id, reply_markup=list_movies)
                        msg_on=0
                        msg_off=1

                    try:
                        if cmd in world_movie_name:
                            get_index = world_movie_name.index(cmd.strip())
                            get_link = movie_download[get_index]

                            # for i in get_link:
                            bot.sendMessage(chatid, "خب اینم زیرنویسی که میخواستی 😇\n\n{}\n\n🤖:@searchsubbot\nبه دوستات منو معرفی کن 🤩".format(
                                get_link), reply_to_message_id=msg_id)


                    except:
                        pass

                if subtitle==1:
                    movie_links = Subtitlepedia(cmd)
                    movie_download = movie_links.search_link()
                    keyboard = [world_movie_name[0:1],
                                world_movie_name[1:2],
                                world_movie_name[2:3],
                                world_movie_name[3:4],
                                world_movie_name[4:5],
                                world_movie_name[5:6],
                                world_movie_name[6:len(subtitlepedia_movie_name)]]

                    list_movies = ReplyKeyboardMarkup(keyboard=keyboard)
                    bot.sendMessage(chatid, "🔥🔥🔥لیست اسم فیلم‌هایی که شبیه به فیلمت هستن 🔥🔥🔥",
                                    reply_to_message_id=msg_id, reply_markup=list_movies)

                    try:
                        if cmd in world_movie_name:
                            get_index = subtitlepedia_movie_name.index(cmd.strip())
                            get_link = movie_download[get_index]

                            # for i in get_link:
                            bot.sendMessage(chatid, "خب اینم زیرنویسی که میخواستی 😇\n\n{}\n\n🤖:@searchsubbot\nبه دوستات منو معرفی کن 🤩".format(get_link), reply_to_message_id=msg_id)

                    except:
                        pass     
        except NoResultFound:
            insert_user = Users(chatid = chatid)
            session.add(insert_user)
            session.commit()



print("Bot working ... ")
MessageLoop(bot, handle).run_as_thread()
while True:
    sleep(10)



# u1 = Search(content="se7en", user_id = 1)
# session.add(u1)
# session.commit()
# # c1 = Search(title="se7en", user_id=(100))
# # session.add(c1)
# # session.commit()
