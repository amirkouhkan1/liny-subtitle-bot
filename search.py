from lxml import html
import requests
from time import sleep


class WorldSubtitle:
    def __init__(self, movie):
        self.movie = movie

    def search_name(self):
        name = self.movie
        
        # URL
        URL = "http://worldsubtitle.us/?s={}".format(str(name).replace(' ','+'))
        page = requests.get(URL)
        page_html = html.fromstring(page.text)
        name_path = page_html.xpath('//div[@class="col-lg-6"]/div[@class="cat-post"]/div[@class="cat-post-bt"]/a/@title')
        return name_path
        
    def search_link(self):
        name = self.movie
        dl_list = []
        # URL
        URL = "http://worldsubtitle.us/?s={}".format(str(name).replace(' ','+'))
        page = requests.get(URL)
        page_html = html.fromstring(page.text)
        link_path = page_html.xpath('//div[@class="col-lg-6"]/div[@class="cat-post"]/div[@class="cat-post-bt"]/a/@href')
        for link in link_path:
            dl_page = requests.get(link)
            dlpage_html = html.fromstring(dl_page.text)
            dllink_path = dlpage_html.xpath('//div[@class="single-box single-dl"]/div[@class="single-box-body"]/div[@id="new-link"]/ul/li/div[@class="new-link-3"]/a/@href')            
            # title_path = dlpage_html.xpath('//div[@class="single-box single-dl"]/div[@class="single-box-body"]/div[@id="new-link"]/ul/li/div[@class="new-link-1"]/text()')
            
            for i in dllink_path:
                dl_list.append(i)

            if len(dllink_path) <= 0:
                dl_page = requests.get(link)
                dlpage_html = html.fromstring(dl_page.text)
                dllink_path = dlpage_html.xpath('//div[@class="single-box single-dl"]/div[@class="single-box-body"]/div[@id="down"]/ul[@class="download1"]/li/a/@href')
                # title_path = dlpage_html.xpath('//div[@class="single-box single-dl"]/div[@class="single-box-body"]/div[@id="down"]/ul[@class="onvan"]/li/text()')
                for i in dllink_path:
                    dl_list.append(i)

        return dl_list
            
class Subtitlepedia:
    def __init__(self, movie):
        self.movie = movie

    def search_name(self):
        name = self.movie
        URL = 'http://subtitlepedia.biz/component/search/?searchword={}&ordering=popular&searchphrase=all&limit=5&areas[0]=categories&areas[1]=jdownloads'.format(name)
        page = requests.get(URL)
        page_html = html.fromstring(page.text)
        name_path = page_html.xpath('//*/dt[@class="result-title"]/a/@title')

        return name_path

    def search_link(self):
        dl_list = []
        name = self.movie
        URL = 'http://subtitlepedia.biz/component/search/?searchword={}&ordering=popular&searchphrase=all&limit=5&areas[0]=categories&areas[1]=jdownloads'.format(name)
        page = requests.get(URL)
        page_html = html.fromstring(page.text)
        link_path = page_html.xpath('//*/dt[@class="result-title"]/a/@href')

        for i in link_path:
            page_dl = requests.get("http://subtitlepedia.biz"+i)
            page_html_dl = html.fromstring(page_dl.text)
            link_path_dl = page_html_dl.xpath('//*/a[@class="jd_download_url"]/@href')

            for j in link_path_dl:
                dl_list.append("http://subtitlepedia.biz"+j)

        return dl_list 



# a = Subtitlepedia("seven")
# print(a.search_name())
a = WorldSubtitle("breaking bad")
print(a.search_link())