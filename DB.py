from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()


class Users(Base):
    __tablename__ = 'tbl_users'
    id = Column(Integer, primary_key=True)
    chatid = Column(Integer, nullable=False)


class Search(Base):
    __tablename__ = 'tbl_search'
    id = Column(Integer, primary_key=True, autoincrement=True)
    content = Column(String(250), nullable=False)
    user_id = Column(Integer, ForeignKey('tbl_users.id'))
    user = relationship(Users)


engine = create_engine('sqlite:///subtitle.db')

Base.metadata.create_all(engine)
